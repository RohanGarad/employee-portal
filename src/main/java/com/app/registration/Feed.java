package com.app.registration;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Feed {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int f_id;
	private String discription;
	private String sender;
	

	public Feed() {
		
	}
	
	
	
	
	public Feed(int f_id, String discription, String from) {
		super();
		this.f_id = f_id;
		this.discription = discription;
		this.sender = from;
	}
	public int getF_id() {
		return f_id;
	}
	public void setF_id(int f_id) {
		this.f_id = f_id;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public String getFrom() {
		return sender;
	}
	public void setFrom(String from) {
		this.sender = from;
	}
	
	

}
