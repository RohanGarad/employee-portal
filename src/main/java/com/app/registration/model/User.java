package com.app.registration.model;


import javax.persistence.Entity;

import javax.persistence.Id;

@Entity
public class User {
	
	@Id
	//@GeneratedValue ( strategy = GenerationType.AUTO)
	 private int id;
	 private String emailId;
	 private String userName;
	 private String password;
	 private String role;
	 private String address;
	 private String phone;
	 private String bdate;
	 
	 
	 
	 



		public User() {
			
		}
	 
	 
	 
	public User(int id, String emailId, String userName, String password, String role, String address, String phone,String bdate) {
		super();
		this.id = id;
		this.emailId = emailId;
		this.userName = userName;
		this.password = password;
		this.role = role;
		this.address=address;
		this.phone=phone;
		this.bdate=bdate;
		
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getPhone() {
		return phone;
	}



	public void setPhone(String phone) {
		this.phone = phone;
	}



	public String getBdate() {
		return bdate;
	}



	public void setBdate(String bdate) {
		this.bdate = bdate;
	}

	 
	 
	 

}
