package com.app.registration.service;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.app.registration.model.User;
import com.app.registration.repository.RegistrationRepository;

//@Component
@Service
public class RegistrationService {
     
	
	
	@Autowired
	private RegistrationRepository repo;
	public List<User> getUser()
	{
		return repo.findAll();
	}
	public User saveUser(User user )
	{
		 return repo.save(user);
		
	}
	public User fetchUserByEmailId(String email)
	{
		 return repo.findByEmailId(email);
	}

	public User fetchUserByEmailIdAndPassword(String email ,String password)
	
	{
		 return repo.findByEmailIdAndPassword(email, password);
	}
	
/*
	public String getUserByEmailId(String emailId) {
	
	 return repo.getByEmailId(emailId);
	}

   */
	
 
}
