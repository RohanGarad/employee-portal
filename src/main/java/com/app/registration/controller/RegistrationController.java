package com.app.registration.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.registration.Feed;
import com.app.registration.FeedRepository;
import com.app.registration.model.User;
import com.app.registration.service.RegistrationService;



@RestController // This means that this class is a Controller
//@Component
public class RegistrationController {
	
	@Autowired
	private RegistrationService service;
	@Autowired
    private FeedRepository feedRepository;
	
	@PostMapping("/registeruser")
	@CrossOrigin(origins = "*")
	public User registerUser(@RequestBody User user) throws Exception
	{
		String tempEmailId=user.getEmailId();
		if(tempEmailId !=null && !"".equals(tempEmailId))
		{
		     User userObj=service.fetchUserByEmailId(tempEmailId);
		     if(userObj!=null)
		     {
		    	 throw new Exception("User With "+tempEmailId+"Is alredy exist");
		     }
		}
		User userObj=null;
		userObj=service.saveUser(user);
	    return userObj;
	}

	@PostMapping("/login")
	@CrossOrigin(origins = "*")
	public User loginUser(@RequestBody User user) throws Exception {
		
		
		String tempEmailId=user.getEmailId();
		String tempPass =user.getPassword();
		User userObj=null;
		
		if(tempEmailId !=null && tempPass != null)
		{
			userObj=service.fetchUserByEmailIdAndPassword(tempEmailId, tempPass);
		
		}
		if(userObj== null)
		{
			throw new Exception("User Not Exist/Bad Credential");
		}
		return userObj;
		
	}
	@GetMapping("/feed")
	@CrossOrigin(origins = "*")
	
	public @ResponseBody Iterable<Feed> getAllFeed()
	{
		return feedRepository.findAll();
		
	}
	
	@PostMapping("/savepost")
	@CrossOrigin(origins = "*")
	public Feed postFeed(@RequestBody Feed feed)
	{
		return feedRepository.save(feed);
		
	}
	
	@PutMapping("/update")
	@CrossOrigin(origins = "*")
	public User userUpdate(@RequestBody User user)
	{
		return service.saveUser(user);
	}
	/*

	@PutMapping("/updatepassword")
	@CrossOrigin(origins = "http://localhost:4200")
	public User updatePassword(@RequestBody User user) throws Exception
	
	{
		String tempemailId = "rohan@gmail.com";
		String temppass ="rohan123";
		User userObj = null;
		if(tempemailId != null)
		{
			userObj= service.fetchUserByEmailId(tempemailId , temppass);
			
			if(userObj == null)
			{
				throw new Exception ("updatea error");
			}
		}
		User userObj1 =null;
		
		userObj1= service.saveUser(userObj);
		return userObj1;
	}
	
	*/
	/*
	@PutMapping("/forgotpass")
	@CrossOrigin(origins = "*")
	public String getByEmailId(@RequestParam(value="emailId" ,required = false) String emailId)
	{
		       
		    this.service.getUserByEmailId(emailId);
		 System.out.println("updated Default Password is rohan1234");
		 System.out.println("Updated");
		 
		 return emailId;
	}
	*/
	
}
	
