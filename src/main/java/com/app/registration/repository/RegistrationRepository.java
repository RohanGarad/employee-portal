package com.app.registration.repository;


import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.registration.model.User;

public interface RegistrationRepository extends JpaRepository < User , Integer>{
	public User findByEmailId(String emailId);
	public User findByEmailIdAndPassword(String emailId , String password);

	/*
	@Modifying
	@Transactional
	@Query(value = "Update user u set u.password='rohan1234' Where u.email_id=:emailId " , nativeQuery =true)
	public String getByEmailId(@Param("emailId") String emailId );
	*/

}
